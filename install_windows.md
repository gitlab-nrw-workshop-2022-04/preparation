# Git Installationsanleitung, Windows

Die Installationsdatei können Sie unter folgender URL herunterladen: https://git-scm.com

![](./img/1_Download.png)

Klicken Sie auf dieser Seite entweder direkt auf den Download-Link auf der rechten Seite unterhalb der aktuellen Release Version oder ...

![](./img/2_Download.png)

... wechseln Sie per Klick auf "Downloads" in den Downloadbereich und wählen dort den passenden Download aus.

![](./img/3_Download.png)

Bestätigen Sie anschließend, dass Sie die Datei speichern möchten. Nach Abschluss des Downloads führen Sie diesen bitte per Doppelklick auf die Datei aus. Nach Start der Installationsroutine müssen Sie bestätigen, dass dieses Programm Änderungen an Ihrem PC vornimmt, anschließend öffnet sich das Installations-Programm.

![](./img/4_Install_cut.png)

Bestätigen Sie die Lizenzinformationen über den Button "Next".

![](./img/5_Install_cut.png)

Auf der folgenden Seite wählen Sie bitte ein Installationsverzeichnis für git aus und bestätigen dies über den Button "Next".

![](./img/6_install_cut.png)

Lassen Sie die ausgewählten Komponenten so, wie sie voreingestellt sind und bestätigen Sie über den Button "Next".

![](./img/7_install_cut.png)

Wählen Sei einen Namen für den Menüeintrag im Startmenü (im Normalfall kann man den voreingestellten Namen verwenden) und bestätigen Sie mit dem Button "Next".

![](./img/8_install_cut.png)

Wählen Sie nun den Texteditor, den git Standarmäßg verwenden soll. Als Empfehlung sollte vim ausgewählt werden, mit dem Hinweis, dass dieser Texteditor eventuell eine kurze Einarbeitung erforderlich macht. Eine Übersicht über die Bedienung von vim und vorhandene Befehle bieten z.B. https://code-bude.net/2013/03/01/vim-einsteiger-tutorial/ oder https://www.grund-wissen.de/linux/shell/vim/bedienung.html.

![](./img/9_install_cut.png)

Wählen Sie auf der nächsten Seite den Punkt „Use Git from the Windows command prompt“ (voreingestellt) und bestätigen Sie mit dem Button "Next".

![](./img/10_install_cut.png)

Übernehmen Sie auch auf der folgenden Seite die voreinstellten Werte und bestätigen Sie über den Button "Next".

![](./img/11_install_cut.png)

Auch auf dieser Seite übernehmen Sie den voreingestellten Wert und fahren über "Next" mit der Installation fort.

![](./img/12_install_cut.png)

Wählen Sie auf der folgenden Seiten den obersten, voreingestellten Punkt und setzen Sie die Installation mit "Next" fort.

![](./img/13_install_cut.png)

Wählen Sie bitte den obersten Punkt, falls er nicht vorausgewählt sein sollte, und fahren Sie mit "Next" fort.

![](./img/14_install_cut.png)

Behalten Sie auch im nächsten Schritt die voreingestellten Werte und setzen Sie die Installation mit "Next" fort.

![](./img/15_install_cut.png)

Lassen Sie die Features im nächsten Schritt abgewählt und setzten Sie die Installation fort.

![](./img/16_install_cut.png)

Warten Sie bis die Installation abgeschlossen ist.

![](./img/17_install_cut.png)

Beenden Sie den Installer über "Finish".

![](./img/18_StartMenue_cut.png)

Im Startmenü finden Sie das neu installierte Git.

![](./img/19_Kontextmenue_cut.png)

Auch im Kontextmenü (rechter Mausklick) finden Sie nun Befehle, um Git zu starten.

Die haben Git erfolgreich installiert!