# Installationsanleitung VSCodium (bzw. VSCode)

VSCodium (bzw. VSCode) ist ein Editor für textartige Dateien mit vielen zusätzlichen Funktionalitäten. Das Programm ist für alle gängigen Betriebssysteme verfügbar. Unter anderem bietet der Editor eine integration von git und kann somit auch als übergreifende grafischer Nutzungsschnittstelle (engl. GUI) für git genutzt werden.

Wenn Sie bereits VSCode installiert haben brauchen Sie nicht zusätzlich VSCodium installieren. Die beiden Anwendungen sind im Wesentlichen identisch. Mehr informationen zu VSCode und VSCodium finden Sie auf der Webseite von VSCodium: https://vscodium.com/#why.

Die Installation von VSCodium ist optional. Um VSCodium installieren und verwenden zu können, benötigen Sie eine vorhandene Git Installation. Die benötigte Installationsdatei können Sie sich auf folgender Website herunterladen: https://vscodium.com/. Wählen Sie dort je nach Betriebssystem die entsprechende Installationsdatei aus:

* Für Windows: VSCodiumUserSetup-x64-1.65.2.exe
* Für MacOS: VSCodium-darwin-x64-1.65.2.zip
* Für Linux beachten Sie bitte die Installationsanweisungen für Ihre Distribution auf der Webseite

Die Installation unter Windows und MacOS ist i.d.R. ohne Administratorrechte möglich.

Es ist wahrscheinlich, dass sie die Versionsnummern am ende des Dateinamens (hier: 1.65.2) unterscheiden. Sie sollten jeweils die aktuellste Version verwenden.

## Installation unter Windows

Führen Sie die Installationsdatei aus. Sie müssen zunächst die Lizenzbedingungen der Software akzeptieren.

![](img/2022-03-25-17-04-21.png)

Wählen Sie dann den Installationpfad auf Ihrem Computer aus. Es wird empfohlen den Pfad nicht zu ändern.

![](img/2022-03-25-17-04-36.png)

Wählen Sie aus ob eine Verknüpfung im Startmenü angelegt werden soll. Es wird empfohlen die Einstellungen nicht zu ändern.

![](img/2022-03-25-17-04-49.png)

Zum Schluss können Sie Wählen ob VSCodium als standard Texteditor verwendet werden soll. Deaktivieren Sie die Checkboxen falls Sie einen anderen Texteditor wie z.B. Notepad++ installiert haben und diesen weiter primär nutzen möchten.

![](img/2022-03-25-17-05-00.png)

Das Setup zeigt Ihnen noch eine Zusammenfassung an. Starten Sie die Installation durch Klick auf den Button `Install`.

![](img/2022-03-25-17-05-17.png)

Öffnen Sie nach der Installation VSCodium. In der Standardeinstellung sollten Sie das Programm im Startmenü finden.

## Installation der Erweiterung `Git Graph`

Installieren Sie nun die Erweiterung `Git Graph`. Klicken Sie dazu in der Menüleiste am linken Fensterrand auf den Menüpunkt `Extensions` ![](img/2022-03-25-20-25-41.png).

Geben Sie Im Suchfeld `git graph` ein und wählen sie das Suchergebnis `Git Graph` vom Entwickler `mhutchie` aus. Drücken Sie den blauen Button `Install`.

![](img/2022-03-25-17-06-36.png)

Damit ist die Installation von VSCodium zusammen mit den notwendigen Erweiterungen abgeschlossen.