# Arbeiten mit GitLab Projekten

Nachdem sie die Informationseinheiten abgeschlossen haben, sollten Sie folgenden Ablauf ausprobieren. Versuchen Sie so weit zu kommen wie möglich. Falls Sie nicht ganz durchkommen: Keine Angst! Die Übung werden wir im Workshop wieder aufgreifen:

* Loggen Sie sich bei [GitLab.com](https://www.gitlab.com) ein
    * Erstellen Sie ein neues Projekt mit dem Namen `gitlab-nrw-wokshop`
    * Initialisieren Sie das Projekt mit einer README Datei
    * Klicken Sie auf die README Datei um den Ihalt der Datei anzusehen
    * Klicken Sie auf den Button "Edit"/"Bearbeiten" und machen Sie eine Änderung an der README Datei im Browser
    * Gehen Sie zurück auf die Projektseite und kopieren sie die URL zum Herunterladen des Projekts unter dem `Clone`-Button. Achten Sie darauf, die HTTPS URL zu kopieren    
* Erstellen Sie eine lokale Kopie um mit dem Repository zu arbeiten
    * Öffnen Sie die Kommandozeile um mit git auf ihrem Computer zu arbeiten.
    * Erstellen Sie einen neuen, leeren Ordner. Zum Beispiel mit dem Kommando `mkdir wokshop`
    * Wechseln Sie in den Ordner mit dem Kommando `cd wokshop`
    * Laden Sie das Projekt mit dem Kommando `git clone [URL]` herunter
    * `git` fragt beim Herunterladen nach Nutzername und Passwort - siehe "[Projekte über HTTPS Clonen](clone.md)" für details.
* Machen Sie eine Änderung in der README Datei, diesmal in der lokalen Kopie des Repositories
    * Committen Sie die Änderung in ihrer lokalen Kopie.
    * Übertragen sie die Änderung mit dem Kommando `git push origin main` an GitLab
    * Überprüfen Sie im Browser ob die Änderung in GitLab angekommen ist.